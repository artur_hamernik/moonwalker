public class Walker implements Moon{
    private int x;
    private int y;
    private int direction;

    public Walker(int x, int y, int direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    @Override
    public void forward(int distance) {
        if(direction == 0){
            y+=distance;
        }
        else if(direction == 90 || direction == -270){
            x+=distance;
        }
        else if(direction == 180 || direction == -180){
            y-=distance;
        }
        else if(direction == 270 || direction == -90){
            x-=distance;
        }
    }

    @Override
    public void backward(int distance) {
        if(direction == 0){
            y-=distance;
        }
        else if(direction == 90 || direction == -270){
            x-=distance;
        }
        else if(direction == 180 || direction == -180){
            y+=distance;
        }
        else if(direction == 270 || direction == -90){
            x+=distance;
        }
    }

    @Override
    public void turnLeft(int rotate) {
        direction -= rotate;
        if(direction == -360){
            direction = 0;
        }
    }

    @Override
    public void turnRight(int rotate) {
        direction += rotate;
        if(direction == 360){
            direction = 0;
        }
    }

    @Override
    public String toString() {
        return "Walker{" +
                "x=" + x +
                ", y=" + y +
                ", direction=" + direction +
                '}';
    }
}
