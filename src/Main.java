public class Main {
    public static void main(String[] args){
        Walker walker = new Walker(0,0,0);
        walker.turnLeft(90);
        walker.forward(2);
        walker.turnRight(90);
        walker.backward(2);
        walker.turnLeft(90);
        walker.forward(1);
        System.out.println(walker.toString());
    }
}
