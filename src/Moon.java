public interface Moon {
    void forward(int distance);
    void backward(int distance);
    void turnLeft(int rotate);
    void turnRight(int rotate);
}
